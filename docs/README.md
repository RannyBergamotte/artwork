## pmOS General artwork Style Guide

To ensure coherent visual communication and consistency we have create this guide outlining some basics that we want to be in our artworks. These are just recommendations and should not make you feel constrained within your artwork and creativity. Innovation and discussions are always welcome.

### Referencing

The project is referenced to as "pmOS" or "postmarketOS"

### Standard License

All artwork in the postmarketOS repositories fall under our standard license (CC-BY-SA) unless otherwise specified.

### Standard Attributes

Used across everything: artwork, web, documents, etc...

<img src=/docs/assets/attributes.svg>

Tip: there is a wide range of default green hues pallet available within Inkscape.

### Goals

Create a simple, clean, and straightforward artwork that fits well with the rest of the design where the artwork is used.

Avoid:

- Bloated artwork that takes heavy resources. keep the size slim with adequate quality.

- Adding artwork that is not related to or doesn't have any significant meaning to postmarketOS.

### Design

An important part of our project is sustainability. A theme could be associated with that by focussing on efficiency, recycling, etc.

While not mandatory, it is recommended to use some greenery hues or accents in artwork to convey postmarketOS attribute.

Modern and current hype trends are welcome and are important to make our project stays up-to-date and familiar. Make some adaptations to have them fit well with the current design.

### Some Notes

- Standard logos, photos, presentation and other materials are available to browse directly within this repository.

- Learn from previous established artwork, like the current postmarketOS website and artwork.

- Some artwork is accompanied with its own further detailed guidelines, such as wallpapers.
